package com.org.beans;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class IntervalWrapper {

	String utcDateTime;
	
	OffsetDateTime pdtDatetime;
	
	Map<Integer, OffsetDateTime> StartIntervalMap = new ConcurrentHashMap<>();
	
	Map<Integer, OffsetDateTime> EndIntervalMap = new ConcurrentHashMap<>();

	public Map<Integer, OffsetDateTime> getStartIntervalMap() {
		return StartIntervalMap;
	}

	public void setStartIntervalMap(Map<Integer, OffsetDateTime> startIntervalMap) {
		StartIntervalMap = startIntervalMap;
	}

	public Map<Integer, OffsetDateTime> getEndIntervalMap() {
		return EndIntervalMap;
	}

	public void setEndIntervalMap(Map<Integer, OffsetDateTime> endIntervalMap) {
		EndIntervalMap = endIntervalMap;
	}
	
	public String getUtcDateTime() {
		return utcDateTime;
	}

	public void setUtcDateTime(String utcDateTime) {
		this.utcDateTime = utcDateTime;
	}

	public OffsetDateTime getPdtDatetime() {
		return pdtDatetime;
	}

	public void setPdtDatetime(OffsetDateTime pdtDatetime) {
		this.pdtDatetime = pdtDatetime;
	}

}
