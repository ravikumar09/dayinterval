package com.org.beans;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DayWiseInterval {

	String utcDateTime;
	OffsetDateTime pdtDatetime;

	Map<Integer, List<OffsetDateTime>> accumulatedIntervalNoAndReadings = new ConcurrentHashMap<>();


	Map<Integer, List<String>> accumulatedIntervalNoAndReadingsForUtc = new ConcurrentHashMap<>();

	IntervalWrapper intervalWrapper;
	
	public Map<Integer, List<String>> getAccumulatedIntervalNoAndReadingsForUtc() {
		return accumulatedIntervalNoAndReadingsForUtc;
	}

	public void setAccumulatedIntervalNoAndReadingsForUtc(
			Map<Integer, List<String>> accumulatedIntervalNoAndReadingsForUtc) {
		this.accumulatedIntervalNoAndReadingsForUtc = accumulatedIntervalNoAndReadingsForUtc;
	}

	

	public IntervalWrapper getIntervalWrapper() {
		return intervalWrapper;
	}

	public void setIntervalWrapper(IntervalWrapper intervalWrapper) {
		this.intervalWrapper = intervalWrapper;
	}

	public String getUtcDateTime() {
		return utcDateTime;
	}

	public void setUtcDateTime(String utcDateTime) {
		this.utcDateTime = utcDateTime;
	}

	public OffsetDateTime getPdtDatetime() {
		return pdtDatetime;
	}

	public void setPdtDatetime(OffsetDateTime pdtDatetime) {
		this.pdtDatetime = pdtDatetime;
	}

	public Map<Integer, List<OffsetDateTime>> getAccumulatedIntervalNoAndReadings() {
		return accumulatedIntervalNoAndReadings;
	}

	public void setAccumulatedIntervalNoAndReadings(
			Map<Integer, List<OffsetDateTime>> accumulatedIntervalNoAndReadings) {
		this.accumulatedIntervalNoAndReadings = accumulatedIntervalNoAndReadings;
	}

	@Override
	public String toString() {
		return "DayWiseInterval [utcDateTime=" + utcDateTime + ", pdtDatetime=" + pdtDatetime
				+ ", accumulatedIntervalNoAndReadings=" + accumulatedIntervalNoAndReadings
				+ ", accumulatedIntervalNoAndReadingsForUtc=" + accumulatedIntervalNoAndReadingsForUtc
				+ ", intervalWrapper=" + intervalWrapper + "]";
	}

	
	
}
