package com.org.beans;

import java.time.OffsetDateTime;
import java.util.List;

public class Interval {

	int intervalNo;
	String intervalStartTime;
	String intervalEndTime;
	List<String> utcReadings;
	List<OffsetDateTime> pdtReading;
	List<Double> energyDelivedList;

	public int getIntervalNo() {
		return intervalNo;
	}

	public void setIntervalNo(int intervalNo) {
		this.intervalNo = intervalNo;
	}

	public String getIntervalStartTime() {
		return intervalStartTime;
	}

	public void setIntervalStartTime(String intervalStartTime) {
		this.intervalStartTime = intervalStartTime;
	}

	public String getIntervalEndTime() {
		return intervalEndTime;
	}

	public void setIntervalEndTime(String intervalEndTime) {
		this.intervalEndTime = intervalEndTime;
	}

	public List<String> getUtcReadings() {
		return utcReadings;
	}

	public void setUtcReadings(List<String> utcReadings) {
		this.utcReadings = utcReadings;
	}

	public List<OffsetDateTime> getPdtReading() {
		return pdtReading;
	}

	public void setPdtReading(List<OffsetDateTime> pdtReading) {
		this.pdtReading = pdtReading;
	}

	public List<Double> getEnergyDelivedList() {
		return energyDelivedList;
	}

	public void setEnergyDelivedList(List<Double> energyDelivedList) {
		this.energyDelivedList = energyDelivedList;
	}

}
