package com.org.pkg1;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.org.beans.Day;
import com.org.beans.DayWiseInterval;
import com.org.beans.IntervalWrapper;

public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UTCToPDTDateConversion obj1=new UTCToPDTDateConversion();	
		
//		String reading1="2019-04-21T12:39:24.309-07:00";
//		String reading2="2019-04-22T00:39:24.309-07:00";
//		String reading3="2019-04-21T00:34:24.309-07:00";
//		String reading4="2019-04-21T01:39:24.309-07:00";
		
		String reading1="2019-04-22T19:21:24.309Z";
		String reading2="2019-04-22T19:32:24.309Z";
		String reading3="2019-04-22T19:34:24.309Z";
		String reading4="2019-04-22T19:36:24.309Z";
		
		String reading5="2019-04-21T00:15:00.309Z";
		String reading6="2019-04-21T00:15:01.309Z";
		String reading7="2019-04-21T00:45:01.309Z";
		String reading8="2019-04-21T00:35:00.309Z";
		
		
		
	//	obj1.fillDay(utcDate, pdtDateTime, dwi, day)
		
		Day day=new Day();
		
		List<String> readingList=new ArrayList<>();
		
		readingList.add(reading1);

		readingList.add(reading2);

		readingList.add(reading3);

		readingList.add(reading4);
		
		readingList.add(reading5);

		readingList.add(reading6);

		readingList.add(reading7);

		readingList.add(reading8);
		
		for(String utcDate:readingList)			
		{
			System.out.println("Reading \t"+utcDate);
			
			OffsetDateTime pdtDateTime=	obj1.utcToPdt(utcDate);
			
			System.out.println("offsetDateTime \t"+pdtDateTime);
			
			
			//got the interval as List for the reading
			List<OffsetDateTime> ofst=obj1.getOffsetDateTimeIntervalList(pdtDateTime);
			//got the map of interval for the reading
			Map<Integer,OffsetDateTime> mapInterval=obj1.getIntervalMap(ofst);
			
			//getting intervalWrapper for the Reading.
			IntervalWrapper iw=obj1.getIntervalWrapper(ofst, utcDate, pdtDateTime);
			
			for(Integer i:iw.getStartIntervalMap().keySet())
			{

		//	System.out.println("key \t"+i +"\t value"+iw.getStartIntervalMap().get(i)
		//			+"end value \t"+iw.getEndIntervalMap().get(i));
			
			
			
			}	
			
			System.out.println("utcDate \t"+utcDate);
			System.out.println("pdtDateTime \t"+pdtDateTime);
			System.out.println("iw \t"+iw);
			System.out.println("day \t"+day);
			
			
			day=obj1.fillDay(utcDate, pdtDateTime,  iw, day);
			
			
				
		}
		
		System.out.println("size of DayMap \t"+day.getDayMap().size());
		
		for(String key:day.getDayMap().keySet())
		{
			System.out.println("Key" +key+"\t value \t"+day.getDayMap().get(key));
		
		}
		
		
		
	}

}
