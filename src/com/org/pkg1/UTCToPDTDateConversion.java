package com.org.pkg1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.org.beans.Day;
import com.org.beans.DayWiseInterval;
import com.org.beans.IntervalWrapper;

public class UTCToPDTDateConversion {

	public static void main(String[] args) {

		UTCToPDTDateConversion obj1=new UTCToPDTDateConversion();	
		
		
		
		String utcTime=obj1.getUTCTimeStamp();
		
		
		
		System.out.println("UTC TIME \t "+ utcTime);
		String pdtTime=obj1.pdtTimeStamp();

		System.out.println("PDT TIME \t "+ pdtTime);
		
		
		System.out.println("\n\n======================");
		
		String reading1="2019-04-20T12:39:24.309-07:00";
		String reading2="2019-04-20T00:39:24.309-07:00";
		String reading3="2019-04-20T00:34:24.309-07:00";
		String reading4="2019-04-20T01:39:24.309-07:00";
		
		
		
		Day day=new Day();
		List<String> readingList=new ArrayList<>();
		
		readingList.add(reading1);

		readingList.add(reading2);

		readingList.add(reading3);

		readingList.add(reading4);
		
		
		List<OffsetDateTime> pdtList=new ArrayList<>();
		
		
		
		
		
		OffsetDateTime pdtDateTime=obj1.utcToPdt(utcTime);
		
		//creating the correspoding pdtoffsetDateTimeList
		for(String r:readingList)
		{
			OffsetDateTime pdtDateTime1=	obj1.utcToPdt(r);
			pdtList.add(pdtDateTime1);
		}
		
		
		
		System.out.println("pdtList size \t"+pdtList.size());
		
		
		System.out.println("\n\n=======================");
		
		//on uncommenting it display List of interval also durin its processgin
 //		Map<Integer,OffsetDateTime> map=obj1.getMap(pdtDateTime);
//		System.out.println(" map object \t"+map);
		
		
		
		
		DayWiseInterval dwiFinal=obj1.getFinal(readingList);
		
		
		
		//getting list of interval of that pdtDateTime
		List<OffsetDateTime> ofst=obj1.pdTOffsetDateTomeToIntervalOffsetList(pdtDateTime);
		
		System.out.println("lst size \t"+ ofst.size());
		
		//converting the List[interval1,interval2,invterval2...,intervalN] 
		//to Map[{0,Interval1},{1,Interval2},{2,interval3}......{N,intervalN}]
		//   
		Map<Integer,OffsetDateTime> mapInterval=obj1.convertOffSetListToMap(ofst);
		
		System.out.println("map size \t"+ mapInterval.size());
		
		//just to see interval Data is there in the map
		for(Integer i:mapInterval.keySet())
		{
			
			//System.out.println(mapInterval.get(i));
		}
		
		
		
		
	//filling the wrapper,
		IntervalWrapper iw=obj1.fillOffSetListToIntervalWrapper(ofst);
		
		//setting the utcDateTime of the readings
		iw.setUtcDateTime(utcTime);
		//setting corresponding pdtDateTime of the Reading
		iw.setPdtDatetime(pdtDateTime);
		
		// just to see size of StartIntervalMap
	//	System.out.println(iw.getStartIntervalMap().size());
		// just to see size of EndIntervalMap
	//   System.out.println(iw.getEndIntervalMap().size());
		
		
		
		
		/*for(Integer i:iw.getStartIntervalMap().keySet())
		{
			System.out.println( "start index \t"+i);
		}
		
		for(Integer i:iw.getEndIntervalMap().keySet())
		{
			System.out.println("endindex " +i);
		}
		*/
		
		DayWiseInterval dwi=new DayWiseInterval();
		
		dwi.setUtcDateTime(iw.getUtcDateTime());
		dwi.setPdtDatetime(iw.getPdtDatetime());
		dwi.setIntervalWrapper(iw);
		
		//getting the readingTimeStamp equivalent pdtTimeStamp  as offSetDateTime 
		//we can use the above dwi.getPdtDatetime(); also
		//OffsetDateTime readingTSequivalentTS=iw.getPdtDatetime();
		OffsetDateTime readingTSequivalentTS=dwi.getPdtDatetime();
		
		
		Duration dr=null;
		
		List<Integer> intervalNoList=new ArrayList<>();
		
		for(int i=0;i<iw.getStartIntervalMap().size() *2;i=i+2)
		{
		
	//		System.out.println("start \t"+iw.getStartIntervalMap().get(i));
			
	//		System.out.println("end \t"+iw.getEndIntervalMap().get(i));
			
			String utcReading=iw.getUtcDateTime();
			
			OffsetDateTime startDateTime=iw.getStartIntervalMap().get(i);
			
			OffsetDateTime endDateTime=iw.getEndIntervalMap().get(i);
					
			OffsetDateTime pdtReadingsDateTime=readingTSequivalentTS;
			
			//pdtTime is between the StartPdtTime and EndPdtTime
		boolean b=pdtReadingsDateTime.compareTo(startDateTime) >= 0 && 
					pdtReadingsDateTime.compareTo(endDateTime) <= 0;
			
		Map<Integer,List<OffsetDateTime>>	accumMap=dwi.getAccumulatedIntervalNoAndReadings();
		
		Map<Integer,List<String>>	accumMapForUTC=dwi.getAccumulatedIntervalNoAndReadingsForUtc();
		
		
		if(b==true)
		{
		
			//System.out.println("---------------------------------------------------------inbetween" + i +" reading valie "+readingTSequivalentTS);
			intervalNoList.add(i);
		
		//	Map<Integer,List<OffsetDateTime>>	accumMap=dwi.getAccumulatedIntervalNoAndReadings();
		
			int size=accumMap.size();
		
			if(size==0)
			{
				List<OffsetDateTime> ldt=new ArrayList<>();
				ldt.add(pdtReadingsDateTime);
				accumMap.put(i, ldt);
				
				List<String> utcList=new ArrayList<>();
				utcList.add(utcReading);
				accumMapForUTC.put(i, utcList);
				
				
			}else
			{
				boolean isIntervalNoAvailable=false;
				for(Integer k:accumMap.keySet())
				{
				
					if(k==i)
					{
						isIntervalNoAvailable=true;
						break;
					}
					else
					{
						k=k+2;
					}
				
				} //end of for loop
			
			if(isIntervalNoAvailable==true)
			{
				//update the map
				
				List<OffsetDateTime> updateList=	accumMap.get(i);
					updateList.add(pdtReadingsDateTime);
					accumMap.put(i, updateList);
			
			
				//update the utcMap also
					List<String> utcList=accumMapForUTC.get(i);
					utcList.add(utcReading);
					accumMapForUTC.put(i, utcList);
		
			}
				else  //
			{
				//create new Entry in the map
				List<OffsetDateTime> ldt=new ArrayList<>();
				ldt.add(pdtReadingsDateTime);
				accumMap.put(i, ldt);
			
				//create new Entry in the UtcMap
				List<String> utcList=new ArrayList<>();
				utcList.add(utcReading);
				accumMapForUTC.put(i, utcList);
				
				
			}
			
			
		}//
		
		
		
		}
		else   // else block is reading isnot Belong to the interval
		{  //uncomment to print when reading doesnot lie in an interval
		//	System.out.println("not in between" + i);
		}
		
		
			
		  // dr=Duration.between(iw.getStartIntervalMap().get(i),iw.getEndIntervalMap().get(i));
		
		} // end of For loop for startIntervalMap.getSize
	//	System.out.println(" dr sec \t"+dr.getSeconds());
		
		
		System.out.println("size of intervalNoList \t"+intervalNoList.size());
		
		
		System.out.println(dwi.getAccumulatedIntervalNoAndReadings().size());
		
		System.out.println(dwi.getAccumulatedIntervalNoAndReadingsForUtc().size());
		
		
		
		
		
		
		
		
	//	day=obj1.fillDay(utcTime, pdtDateTime, dwi,IntervalWrapper iw, day);
		
		
		
	}
	

	String pdtTimeStamp()
	{
		ZoneId pdt=ZoneId.of("UTC-8");
		
		OffsetDateTime ofdtpdt=OffsetDateTime.now(pdt);
		
		return ofdtpdt.toString();
	}
	

	
	String getUTCTimeStamp()
	{
		ZoneId pdt=ZoneId.of("UTC");
		
		OffsetDateTime ofdtpdt=OffsetDateTime.now(pdt);
		
		return ofdtpdt.toString();
	}
	

	OffsetDateTime utcToPdt(String utcTimeStamp)
	{
		System.out.println("incoming utcts \t"+utcTimeStamp);
	
		DateTimeFormatter dtf=DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		
	
		OffsetDateTime odf=OffsetDateTime.parse(utcTimeStamp);
		
		System.out.println("utc String date as Zulu OffsetDateTime  ----\t "+odf );
		
		//1
		ZoneId pdt=ZoneId.of("UTC-7");
		//2
	//	OffsetDateTime ofdtpdt=OffsetDateTime.now(pdt);
	
		OffsetDateTime ofdtpdt=null;
		
		ofdtpdt=odf.atZoneSameInstant(pdt).toOffsetDateTime();
		
		System.out.println("utc String date as pdt offsetDateTime\t "+ofdtpdt);	
	
		return ofdtpdt;
	}
	
	
	
	Map<Integer,OffsetDateTime> getMap(OffsetDateTime ofsdt)
	{
		
		
		System.out.println("Inside interval Map \t"+ofsdt);
		
		
		int getHour=ofsdt.getHour();
		int getMin=ofsdt.getMinute();
		int getSec=ofsdt.getMinute();
		int getMili=ofsdt.getNano();
		
		
		
		//ofsdt.mi
		
		System.out.println("Hour \t min \t sec \t"+getHour +" "+ getMin +" "+getSec);
		
				
		OffsetDateTime pdtDateTimeMinusHour=ofsdt.minusHours(getHour);

//		System.out.println("hour sub \t" +pdtDateTimeMinusHour);

		OffsetDateTime pdtDateTimeMinusHourMin=pdtDateTimeMinusHour.minusMinutes(getMin);
//		System.out.println("min sub \t" + pdtDateTimeMinusHourMin);
		

		OffsetDateTime pdtDateTimeMinusHourMinSec=pdtDateTimeMinusHourMin.minusSeconds(getSec);
//		System.out.println(" sec sub \t" + pdtDateTimeMinusHourMinSec);
		
		OffsetDateTime pdtDateTimeMinusHourMinSecMinNano=pdtDateTimeMinusHourMin.minusNanos(getMili);
	//	System.out.println(" Nano sub \t" + pdtDateTimeMinusHourMinSecMinNano);
		
		
		System.out.println(" \n After substraction \t"+pdtDateTimeMinusHourMinSec);
		
		

	/*	ZoneId pdt=ZoneId.of("UTC-7");
		
		
		ZonedDateTime zzdt=pdtDateTimeMinusHourMinSecMinNano.atZoneSimilarLocal(pdt);
		*/
				
		
		
		LocalDateTime pdtLocalDateTime=pdtDateTimeMinusHourMinSecMinNano.toLocalDateTime();
	
		System.out.println("pdt as LocalDateTime without Offset"+pdtDateTimeMinusHourMinSecMinNano.toLocalDateTime());
		
		ZoneOffset zst=ZoneOffset.ofHours(-7);
		System.out.println("\n");
		
		
		
		
		OffsetDateTime pdtOffSetDateTime=OffsetDateTime.of(pdtLocalDateTime,zst);
		
		System.out.println("pdt as LocalDateTime with offsetDateTime \t"+pdtOffSetDateTime.toString());
		
		
		createPDTIntervalMap(pdtOffSetDateTime,pdtLocalDateTime);
		
		
		
		

		
		
		
		
		
//		for(int i=1;i<=6;i++)
//		{
//			OffsetDateTime pdtDateTimeWith15MinInterval=pdtDateTimeMinusHourMinSecMinNano.plusMinutes(15);
//			intervalMap.put(i, pdtDateTimeWith15MinInterval);
//			
//		}
//				
//		for(Integer i:intervalMap.keySet())
//		{
//			System.out.println(intervalMap.get(i));
//		}
//		
//		
//		
		
		Calendar cal = Calendar.getInstance();
		
	//	System.out.println(cal);
		
	//System.out.println(	cal.getInstance());
	

		
		return null;
	}
	
	

	Map<Integer,OffsetDateTime> createPDTIntervalMap(OffsetDateTime pdtOffSetDateTime,LocalDateTime pdtLocalDateTime)
	{
		
		System.out.println("Inside Map for  \t"+ pdtOffSetDateTime);
		
		ZoneId pdt=ZoneId.of("UTC-7");
		
		LocalDate ld=pdtOffSetDateTime.toLocalDate();
		
		ZonedDateTime lt=ld.atStartOfDay(pdt);
		
		System.out.println("zoned date \t"+lt);
		
		System.out.println(	"Zoned date as OffSetDateTime offsetDatetime t"+lt.toOffsetDateTime());

		System.out.println("zdt  zdt "+lt);
	
	
	
		
//	OffsetDateTime offsetASSStarOftheDay=pdtOffSetDateTime.as
	
		Map<Integer,String> intervalMap=new ConcurrentHashMap<>();
		
		
		int gapInMinutes =  15 ;  // Define your span-of-time.
		int loops = ( (int) Duration.ofHours( 24 ).toMinutes() / gapInMinutes ) ;
		List<LocalTime> times = new ArrayList<>( loops ) ;

		LocalTime time = LocalTime.MIN ;  // '00:00'
		
		int secToBeAdded=1;          //loops
		
		
		
		for( int i = 1 ; i <= loops; i ++ ) {			
			time=time.plusSeconds(secToBeAdded);			
		    times.add( time ) ;
		    // Set up next loop.
		    time=time.minusSeconds(1);			
		    time=time.plusMinutes(gapInMinutes);
		    times.add(time);		    
			//sec=(gapInMinutes*60)-1;
		}

		System.out.println( times.size() + " time slots: " ) ;
//		System.out.println( times ) ;
		System.out.println("\n\n After change");
		
	
		List<ZonedDateTime> ldtlist=new ArrayList<>();
		
		System.out.println("size "+ldtlist.size());
		
		//lt
		for(int i=0;i<loops;i++)
		{
			lt=lt.plusSeconds(1);
			ldtlist.add(lt);
			lt=lt.minusSeconds(1);
			
			lt=lt.plusMinutes(gapInMinutes);
			
			ldtlist.add(lt);
			
		}
		
		
//		for(int i=0;i<ldtlist.size();i++)
//		{
//			//System.out.println(ldtlist.get(i));
//		}
//		
		
		
		
		System.out.println("size "+ldtlist.size());
		
		
		/*time=time.plusSeconds(secToBeAdded);		
	    times.add( time ) ;
	    // Set up next loop.
	    time = time.plusSeconds(leftSec);
		*/
		
	List<OffsetDateTime> ofst=zonedListToOffsetList(ldtlist);
	
	
//	System.out.println(ofst);
		
		
	for(int i=0;i<ofst.size();i++)
	{
	//	System.out.println(ofst.get(i));
	}
	
	
	
		Map<Integer,OffsetDateTime> intervalMap2=new ConcurrentHashMap<>();
		
		Map<Integer,String> iM=new ConcurrentHashMap<>();
		
		
		
		
		
		return null;
	}
	
	List<OffsetDateTime> zonedListToOffsetList(List<ZonedDateTime> zdlt)
	{
		
		
		
		List<OffsetDateTime> ofst=new ArrayList<>(zdlt.size());
		
		for(int i=0;i<zdlt.size();i++)
		{
			ZonedDateTime zdt=zdlt.get(i);
			ofst.add(zdt.toOffsetDateTime());
			
		}
		
		return ofst;
	}
	
	
	List<OffsetDateTime> pdTOffsetDateTomeToIntervalOffsetList(OffsetDateTime pdtOffSetDateTime)
	{
		//getting the localDAte
		LocalDate ld=pdtOffSetDateTime.toLocalDate();
		
		//using zoneId
		ZoneId pdt=ZoneId.of("UTC-7");
		//converting localDate to ZonedDateTime
		ZonedDateTime lt=ld.atStartOfDay(pdt);
		//getting zonedDateTime as OffsetDateTime
		OffsetDateTime time=lt.toOffsetDateTime();
	
		
		int gapInMinutes =  15 ;  // Define your span-of-time.
		int loops = ( (int) Duration.ofHours( 24 ).toMinutes() / gapInMinutes ) ;
		
		
		List<OffsetDateTime> times=new ArrayList<>();
		
		int secToBeAdded=1;
		
		for( int i = 1 ; i <= loops; i ++ ) {			
			time=time.plusSeconds(secToBeAdded);
			
		    times.add( time ) ;
		    // Set up next loop.
		    time=time.minusSeconds(secToBeAdded);			
		    time=time.plusMinutes(gapInMinutes);
		    times.add(time);		    
			//sec=(gapInMinutes*60)-1;
		}
		

		System.out.println("Times sizes \t"+times.size());
		
//		List<OffsetDateTime> ofst=new ArrayList<>(zdlt.size());
//		
//		for(int i=0;i<zdlt.size();i++)
//		{
//			ZonedDateTime zdt=zdlt.get(i);
//			ofst.add(zdt.toOffsetDateTime());
//			
//		}
		
		
	//	return ofst;
	return times;
	}
	
	
	Map<Integer,OffsetDateTime> convertOffSetListToMap(List<OffsetDateTime> ofst)
	{
		
		
		Map<Integer,OffsetDateTime> map=new ConcurrentHashMap<>();
		
		
		for(int i=0;i<ofst.size();i++)
		{
			map.put(i, ofst.get(i));
		}
		
		
		return map;
	}

	IntervalWrapper fillOffSetListToIntervalWrapper(List<OffsetDateTime> ofst)
	{
		
		//creating intervalWrapper
		IntervalWrapper iw=new IntervalWrapper();
		
		
		for(int i=0;i<ofst.size();i++)
		{
		
			if(i%2==0)
			{//filling the start interval
				iw.getStartIntervalMap().put(i, ofst.get(i));
			}else
			{
				//filling the end interval
				iw.getEndIntervalMap().put(i-1, ofst.get(i));
					
			}
			
			
		}
		
		//returning the IntervalWrapper
		return iw;
	}
	
	
	
	DayWiseInterval getFinal(List<String> readingList)
	{
		
		DayWiseInterval dwi=new DayWiseInterval();
		
		for(String reading:readingList)
		{
			//getting the pdt as offsetDateTime
			OffsetDateTime pdtDateTime=utcToPdt(reading);
			//got the interval as List for the reading
			List<OffsetDateTime> ofst=getOffsetDateTimeIntervalList(pdtDateTime);
			//got the map of interval for the reading
			Map<Integer,OffsetDateTime> mapInterval=getIntervalMap(ofst);
			
			//getting intervalWrapper for the Reading.
			IntervalWrapper iw=getIntervalWrapper(ofst, reading, pdtDateTime);
			
	
		}
			
		
		
		return null;
	}
	
	IntervalWrapper getIntervalWrapper(List<OffsetDateTime> ofst,String utcReading,OffsetDateTime pdtTimeOfreading)
	{
		
		//filling the iw with startIntervalMap and endIntervalMap by 
		// using the List of OffsetDateTime as even mean start and odd means end 
		// time
		IntervalWrapper iw=fillOffSetListToIntervalWrapper(ofst);
		
		//setting the utcDateTime of the readings
		iw.setUtcDateTime(utcReading);
		//setting corresponding pdtDateTime of the Reading
		iw.setPdtDatetime(pdtTimeOfreading);
		
		return iw;
	}
	
	List<OffsetDateTime> getOffsetDateTimeIntervalList(OffsetDateTime pdtDateTime)
	{
		//getting list of interval of that pdtDateTime
		List<OffsetDateTime> ofst=pdTOffsetDateTomeToIntervalOffsetList(pdtDateTime);
		
		//System.out.println("lst size \t"+ ofst.size());
		
		return ofst;
	}
	
	Map<Integer,OffsetDateTime> getIntervalMap( List<OffsetDateTime> ofstList)
	{
		
		//converting the List[interval1,interval2,invterval2...,intervalN] 
		//to Map[{0,Interval1},{1,Interval2},{2,interval3}......{N,intervalN}]
		//   
		Map<Integer,OffsetDateTime> mapInterval=convertOffSetListToMap(ofstList);
		
		
		return mapInterval;
	}
	
	
	Day fillDay(String utcDate,OffsetDateTime pdtDateTime,IntervalWrapper iw,Day day)
	{
		
		Map<String,DayWiseInterval> dayMap=day.getDayMap();
		
		
		// dayMap for First Time is Empty,The Fill it
		if(dayMap.size()==0)
		{
		//	System.out.println("Executing 0th Time");
			
			String utcDateSplit[]=utcDate.split("T");
			
			String DayDate=utcDateSplit[0];
			
		//	String pdtDateTimeSplit[]=pdtDateTime.toString().split("T");
			
		//	String DayDate=pdtDateTimeSplit[0];
			
			//creating dayWiseInterval Object for a Reading.
			DayWiseInterval dwiFirstTime=new DayWiseInterval();
			
			dwiFirstTime.setUtcDateTime(utcDate);
			dwiFirstTime.setPdtDatetime(pdtDateTime);
			dwiFirstTime.setIntervalWrapper(iw);
			
			// then take the pdtDateTime and use 
			// the dayWiseInterval object, to find in which Interval it lies
			// call a method which place pdtTime object into its suitable interval
			dwiFirstTime=fillPdtReadingIntoDayWiseInterval(dwiFirstTime, pdtDateTime, utcDate);
			
			// updating the Day map with updated
			// dwi object with intervalWrapper
			dayMap.put(DayDate, dwiFirstTime);
			
			
	//		System.out.println("Size of dayMap \t"+dayMap.size());
			
			return day;
		}
		else
		{
			//when 2nd utcDate with Reading is coming
			// check the incoming date is already there in the dayMap or Not
			
			String utcDateSplit[]=utcDate.split("T");
			String incomingDayDate=utcDateSplit[0];
			
		//	String pdtDateTimeSplit[]=pdtDateTime.toString().split("T");
		//	String incomingDayDate=pdtDateTimeSplit[0];
			
			boolean isIncomingDateAvailableInDayMap=false;
			
			String keyDate="null";
			
			for(String existingDateInMap: dayMap.keySet())
			{
		//		System.out.println("MapExisstin date \t"+existingDateInMap);
		//		System.out.println("incoming Date \t"+incomingDayDate);
				if(existingDateInMap.equalsIgnoreCase(incomingDayDate))
				{
					isIncomingDateAvailableInDayMap=true;
					keyDate=existingDateInMap;
					break;
				}
				keyDate=existingDateInMap;
			}
			
			//when incoming date is Already Available
			// it means its dayWiseInterval Object is also Available
			if(isIncomingDateAvailableInDayMap==true)
			{
				
				DayWiseInterval dwi=day.getDayMap().get(keyDate);
			//	System.out.println("DWI object \t"+dwi + " keyvalue \t"+ keyDate);
				// then take the pdtDateTime and use 
				// the dayWiseInterval object, to find in which Interval it lies
				// call a method which place pdtTime object into its suitable interval
				dwi=fillPdtReadingIntoDayWiseInterval(dwi, pdtDateTime, utcDate);
				
				// updating the Day map with updated
				// dwi object with intervalWrapper
				dayMap.put(incomingDayDate, dwi);
				return day;
				
			}
			else //if incoming date is a new Day realted 
				 // then create a new entry in the Day map
				 // 1.create the new Dwi object also to put it in the map
			{   // 2. create the new intervalMap of the new Dwi object and 
				// 3. set this intervalMap into dwi object
				// 4. after filling the dwi object, now use dwi
				// to accumulate the new Reading into dwi maps
				// 
				
				System.out.println("UtcDate for New Reading\t "+utcDate);
				
				System.out.println("pdtDate for New Reading \t"+pdtDateTime);
				
				// List of OffsetDateTime with its interval
				List<OffsetDateTime> ofst=getOffsetDateTimeIntervalList(pdtDateTime);
			
				for(OffsetDateTime od:ofst)
				{
				//	System.out.println(od);
				}
				
				
				
				
				// fully filled interval Wrapper	
				IntervalWrapper newiw=getIntervalWrapper(ofst, utcDate, pdtDateTime);
				
				//creating the dwi object
				DayWiseInterval newdwi=new DayWiseInterval();
				
				newdwi.setUtcDateTime(newiw.getUtcDateTime());
				newdwi.setPdtDatetime(newiw.getPdtDatetime());
				newdwi.setIntervalWrapper(newiw);
			
				// then take this new pdtDateTime and use 
				// the dayWiseInterval object, to find in which Interval it lies
				// call a method which place pdtTime object into its suitable interval
				newdwi=fillPdtReadingIntoDayWiseInterval(newdwi, pdtDateTime, utcDate);
				
				//putting the newDate in the DayMap
				dayMap.put(incomingDayDate, newdwi);
				
			} //end of isincomingDateAvailableInMap
		
		
		
		} // else block end
		
		
		return day;
	}
	
	
	DayWiseInterval fillPdtReadingIntoDayWiseInterval(DayWiseInterval dwi,OffsetDateTime pdtTime,String utcTime)
	{
		
		
		//1. get the intervalMap from dwi object
		IntervalWrapper iw=dwi.getIntervalWrapper();
		
		//setting the utcDateTime of the readings
		iw.setUtcDateTime(utcTime);
		//setting corresponding pdtDateTime of the Reading
		iw.setPdtDatetime(pdtTime);
		
		
		dwi.setUtcDateTime(iw.getUtcDateTime());
		dwi.setPdtDatetime(iw.getPdtDatetime());
		dwi.setIntervalWrapper(iw);
		
		for(int i=0;i<iw.getStartIntervalMap().size() *2;i=i+2)
		{
		
	//		System.out.println("start \t"+iw.getStartIntervalMap().get(i));
			
	//		System.out.println("end \t"+iw.getEndIntervalMap().get(i));
			
			String utcReading=iw.getUtcDateTime();
			
			OffsetDateTime startDateTime=iw.getStartIntervalMap().get(i);
			
			OffsetDateTime endDateTime=iw.getEndIntervalMap().get(i);
					
		//	System.out.println("startDAte"+startDateTime);
			
		//	System.out.println("pdtDateTime \t"+pdtTime);
			
			//		System.out.println("END DATE \t"+endDateTime);
			OffsetDateTime pdtReadingsDateTime=format(pdtTime);
			
			//pdtTime is between the StartPdtTime and EndPdtTime
		  boolean b=pdtReadingsDateTime.compareTo(startDateTime) >= 0 && 
					pdtReadingsDateTime.compareTo(endDateTime) <= 0;
			
		Map<Integer,List<OffsetDateTime>>	accumMap=dwi.getAccumulatedIntervalNoAndReadings();
		
		Map<Integer,List<String>>	accumMapForUTC=dwi.getAccumulatedIntervalNoAndReadingsForUtc();
		
		
		if(b==true)
		{
		
		System.out.println("InBetween Available 000000000000000000000000000000000000000000000000");
		//	Map<Integer,List<OffsetDateTime>>	accumMap=dwi.getAccumulatedIntervalNoAndReadings();
		
				System.out.println("startDAte"+startDateTime);
		
				System.out.println("pdtDateTime \t"+pdtTime);
				
				System.out.println("END DATE \t"+endDateTime);
				
		
		
		
			int size=accumMap.size();
		
			if(size==0)
			{
				List<OffsetDateTime> ldt=new ArrayList<>();
				ldt.add(pdtReadingsDateTime);
				accumMap.put(i, ldt);
				
				List<String> utcList=new ArrayList<>();
				utcList.add(utcReading);
				accumMapForUTC.put(i, utcList);
				
				
			}else
			{
				boolean isIntervalNoAvailable=false;
				for(Integer k:accumMap.keySet())
				{
				
					if(k==i)
					{
						isIntervalNoAvailable=true;
						break;
					}
					else
					{
						k=k+2;
					}
				
				} //end of for loop
			
			if(isIntervalNoAvailable==true)
			{
				//update the map
				
				List<OffsetDateTime> updateList=	accumMap.get(i);
					updateList.add(pdtReadingsDateTime);
					accumMap.put(i, updateList);
			
			
				//update the utcMap also
					List<String> utcList=accumMapForUTC.get(i);
					utcList.add(utcReading);
					accumMapForUTC.put(i, utcList);
		
			}
				else  //
			{
				//create new Entry in the map
				List<OffsetDateTime> ldt=new ArrayList<>();
				ldt.add(pdtReadingsDateTime);
				accumMap.put(i, ldt);
			
				//create new Entry in the UtcMap
				List<String> utcList=new ArrayList<>();
				utcList.add(utcReading);
				accumMapForUTC.put(i, utcList);
				
				
			}
			
			
		}//
		
		
		
		}
		else   // else block is reading isnot Belong to the interval
		{  //uncomment to print when reading doesnot lie in an interval
		  //	System.out.println("Not MAtching");
		}
		
		
			
		  // dr=Duration.between(iw.getStartIntervalMap().get(i),iw.getEndIntervalMap().get(i));
		
		} // end of For loop for startIntervalMap.getSize
		
		
		return dwi;
	} //end of FillpdtReadingIntoDayWiseInterval()
	
	
	OffsetDateTime format(OffsetDateTime ost)
	{
		
		
		String incomingDate=ost.toString();
		
							//"yyyy-MM-dd'T'HH:mm.ss"
		DateTimeFormatter dtf1=DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss-07:00");
		
	
		String s=ost.format(dtf1);
		
		DateTimeFormatter dtf2=DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
		

	//	System.out.println("after format\t"+s);
		
		//OffsetDateTime.parse(s, formatter)	
		
		
	//System.out.println(	ost.parse(s));
		
		OffsetDateTime ii=ost.parse(s);
	
//		System.out.println("iii \t"+ii);
	//	System.out.println("odf value \t"+odf);
		return ii;
		
	//	return odf;
	}
	
	
}






/*System.out.println("zzdt \t"+ zzdt);

String offSetDt=pdtDateTimeMinusHourMinSecMinNano.toString();

("offset  dateTime as String \t" +offSetDt);

*/


/*LocalDate ldt=zzdt.toLocalDate();

LocalDateTime ldtm=zzdt.toLocalDateTime();

System.out.println(zzdt);

System.out.println(ldt);

System.out.println("ldtm1 " +ldtm);

String ldtmm=ldtm.toString();

String ldtf="yyyy-MM-dd'T'HH:mm:ss";	

DateTimeFormatter dtf=DateTimeFormatter.ofPattern(ldtf);

LocalDateTime ldtm1=LocalDateTime.parse(ldtmm, dtf);

System.out.println("ldtm1 \t"+ldtm1);

*/

